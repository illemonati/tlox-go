BINARY_NAME=tlox-go

build:
	go mod tidy
	go build -o build/${BINARY_NAME} ./src/

clean:
	go clean
	rm -r build