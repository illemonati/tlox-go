package main

import (
	"fmt"
	"strconv"
	"unicode"
)

type Scanner struct {
	source []rune
	tokens []Token

	start   int
	current int
	line    int
}

func MakeScanner(sourceStr string) Scanner {
	return Scanner{
		source: []rune(sourceStr),
		tokens: []Token{},

		start:   0,
		current: 0,
		line:    1,
	}
}

func (s *Scanner) atEnd() bool {
	return s.current >= len(s.source)
}

func (s *Scanner) ScanTokens() []Token {
	for !s.atEnd() {
		// at start of next lexeme
		s.start = s.current
		s.scanToken()

	}

	s.tokens = append(s.tokens, MakeToken(EOF, "", nil, s.line))
	return s.tokens
}

func (s *Scanner) readChar() rune {
	c := s.source[s.current]
	s.current++
	return c
}

func (s *Scanner) addToken(tokenType TokenType, literal any) {
	lexeme := string(s.source[s.start:s.current])
	s.tokens = append(s.tokens, MakeToken(tokenType, lexeme, literal, s.line))
}

func (s *Scanner) addNilLiteralToken(tokenType TokenType) {
	s.addToken(tokenType, nil)
}

func (s *Scanner) ifMatchThenConsume(expectedCharacter rune) (matches bool) {
	if s.atEnd() {
		return
	}
	if s.source[s.current] != expectedCharacter {
		return
	}

	s.current++
	matches = true
	return
}

func (s *Scanner) ifMatchThenConsumeAndOutputTokenType(expectedCharacter rune, option1, option2 TokenType) (result TokenType) {
	result = option1
	if !s.ifMatchThenConsume(expectedCharacter) {
		result = option2
	}
	return
}

func (s *Scanner) peek() rune {
	return s.peekNPastCurrent(0)
}

func (s *Scanner) peekNPastCurrent(n int) rune {
	if s.current+n >= len(s.source) {
		return '\x00'
	}
	return s.source[s.current+n]
}

func (s *Scanner) scanToken() {
	c := s.readChar()
	switch c {

	// single chars
	case '(':
		s.addNilLiteralToken(LEFT_PAREN)
	case ')':
		s.addNilLiteralToken(RIGHT_PAREN)
	case '{':
		s.addNilLiteralToken(LEFT_BRACE)
	case '}':
		s.addNilLiteralToken(RIGHT_BRACE)
	case ',':
		s.addNilLiteralToken(COMMA)
	case '.':
		s.addNilLiteralToken(DOT)
	case '-':
		s.addNilLiteralToken(MINUS)
	case '+':
		s.addNilLiteralToken(PLUS)
	case ';':
		s.addNilLiteralToken(SEMICOLON)
	case '*':
		s.addNilLiteralToken(STAR)

	// possible double chars
	case '!':
		s.addNilLiteralToken(s.ifMatchThenConsumeAndOutputTokenType('=', BANG_EQUAL, BANG))
	case '=':
		s.addNilLiteralToken(s.ifMatchThenConsumeAndOutputTokenType('=', EQUAL_EQUAL, EQUAL))
	case '<':
		s.addNilLiteralToken(s.ifMatchThenConsumeAndOutputTokenType('=', LESS_EQUAL, LESS))
	case '>':
		s.addNilLiteralToken(s.ifMatchThenConsumeAndOutputTokenType('=', GREATER_EQUAL, GREATER))

	// comment and divide
	case '/':
		if s.ifMatchThenConsume('/') {
			// this is a comment
			for s.peek() != '\n' && !s.atEnd() {
				s.current++
			}
		} else {
			s.addNilLiteralToken(SLASH)
		}

	//white space
	case ' ', '\r', '\t':
		// do nothing
	case '\n':
		s.line++

	// strings:
	case '"':
		s.handleString()

	default:

		// numbers
		if unicode.IsDigit(c) {
			s.handleNumber()
			break
		}

		// identifiers start with letters or _
		if unicode.IsLetter(c) || c == '_' {
			s.handleIdentifier()
			break
		}

		Error(s.line, "Unexpected character.")
	}
}

func (s *Scanner) handleIdentifier() {
	for {
		c := s.peek()
		if !unicode.IsLetter(c) && !unicode.IsNumber(c) && c != '_' {
			break
		}
		s.current++
	}

	identText := string(s.source[s.start:s.current])
	tokenType := EOF
	literal := any(nil)
	if val, ok := keywords[identText]; ok {
		switch val {
		case TRUE:
			literal = true
		case FALSE:
			literal = false
		}
		tokenType = val
	} else {
		tokenType = IDENTIFIER
	}
	s.addToken(tokenType, literal)
}

func (s *Scanner) handleNumber() {
	readDigits := (func() {
		for unicode.IsDigit(s.peek()) {
			s.current++
		}
	})

	readDigits()

	// handle decimal
	if s.peek() == '.' && unicode.IsDigit(s.peekNPastCurrent(1)) {
		s.current++
		readDigits()
	}

	num, err := strconv.ParseFloat(string(s.source[s.start:s.current]), 64)

	if err != nil {
		Error(s.line, fmt.Sprintf("Cannot Parse number: %v.", err))
	}

	s.addToken(NUMBER, num)
}

func (s *Scanner) handleString() {
	for {
		c := s.peek()
		if c == '"' || s.atEnd() {
			break
		}
		if c == '\n' {
			s.line++
		}
		s.current++
	}

	if s.atEnd() {
		Error(s.line, "Unterminated string.")
	}

	// handle closing "
	s.current++

	// add string to tokens
	s.addToken(STRING, string(s.source[s.start+1:s.current-1]))
}
