package main

import "fmt"

type Evaluator struct {
	interpreter *Interpreter
}

func NewEvaluator(interpreter *Interpreter) *Evaluator {
	return &Evaluator{
		interpreter,
	}
}

func (e *Evaluator) evalAssign(expr *AssignExpression) any {
	val := e.Evaluate(expr.value)
	e.interpreter.environment.Assign(expr.name, val)
	return val
}

func (e *Evaluator) evalVariable(expr *VariableExpression) any {
	return e.interpreter.environment.GetVariableValue(expr.name)
}

func (e *Evaluator) evalLiteral(expr *LiteralExpression) any {
	return expr.value
}

func (e *Evaluator) evalGrouping(expr *GroupingExpression) any {
	return e.Evaluate(expr.innerExpression)
}

func (e *Evaluator) evalUnaryExpr(expr *UnaryExpression) any {
	right := e.Evaluate(expr.right)
	switch expr.operator.tokenType {
	case MINUS:
		if r, ok := right.(float64); ok {
			return -r
		}
	case BANG:
		return !e.isTruthy(right)
	default:
		Error(-1, "Unknown unary, this is a bug.")
	}
	return nil
}

func (e *Evaluator) evalFunctionCallExpr(expr *FunctionCallExpression) any {
	callee := e.Evaluate(expr.callee)

	arguments := make([]any, 0)
	for _, arg := range expr.arguments {
		arguments = append(arguments, e.Evaluate(arg))
	}

	function, ok := callee.(Callable)
	if !ok {
		Error(expr.paren.line, "Callee not callable.")
	}

	if function.Arity() != len(arguments) {
		Error(expr.paren.line, fmt.Sprintf("Expected %d arguments, received %d.", function.Arity(), len(arguments)))
	}
	return function.Call(e.interpreter, arguments)
}

func (e *Evaluator) evalBinaryExpr(expr *BinaryExpression) any {
	left := e.Evaluate(expr.left)
	right := e.Evaluate(expr.right)

	leftNum, leftNumeric := left.(float64)
	rightNum, rightNumeric := right.(float64)

	leftRightBothNumeric := leftNumeric && rightNumeric

	checkNumeric := (func() {
		if !leftRightBothNumeric {
			Error(expr.operator.line, "This binary expression require both arguments to be numeric.")
		}
	})

	switch expr.operator.tokenType {

	// arithmic
	case MINUS:
		checkNumeric()
		return leftNum - rightNum
	case SLASH:
		checkNumeric()
		return leftNum / rightNum
	case STAR:
		checkNumeric()
		return leftNum * rightNum
	case PLUS:
		if leftRightBothNumeric {
			return leftNum + rightNum
		} else {
			lString, lIsStr := left.(string)
			rString, rIsStr := right.(string)
			if lIsStr && rIsStr {
				return fmt.Sprintf("%s%s", lString, rString)
			} else {
				if lIsStr && rightNumeric {
					return lString + NumToString(rightNum)
				} else if rIsStr && leftNumeric {
					return NumToString(leftNum) + rString
				} else {
					Error(expr.operator.line, "PLUS requires string or numerical arguments.")
				}
			}
		}

	// comparison
	case GREATER:
		checkNumeric()
		return leftNum > rightNum
	case GREATER_EQUAL:
		checkNumeric()
		return leftNum >= rightNum
	case LESS:
		checkNumeric()
		return leftNum < rightNum
	case LESS_EQUAL:
		checkNumeric()
		return leftNum <= rightNum

	// equality
	case BANG_EQUAL:
		return left != right
	case EQUAL_EQUAL:
		return left == right

	// logical
	case OR:
		if e.isTruthy(left) {
			return left
		} else {
			return right
		}

	case AND:
		if e.isTruthy(left) {
			return right
		} else {
			return left
		}

	}

	return nil
}

func (e *Evaluator) isTruthy(val any) bool {
	if val == nil {
		return false
	}
	switch t := val.(type) {
	case bool:
		return t
	case float64:
		return t == 0
	default:
		return val != nil
	}
}

func (e *Evaluator) Evaluate(expr Expression) (result any) {
	switch t := expr.(type) {
	case *LiteralExpression:
		result = e.evalLiteral(t)
	case *GroupingExpression:
		result = e.evalGrouping(t)
	case *BinaryExpression:
		result = e.evalBinaryExpr(t)
	case *UnaryExpression:
		result = e.evalUnaryExpr(t)
	case *VariableExpression:
		result = e.evalVariable(t)
	case *AssignExpression:
		result = e.evalAssign(t)
	case *FunctionCallExpression:
		result = e.evalFunctionCallExpr(t)
	default:
		Error(-1, "Unknown expression type, this is a bug.")
	}
	return
}
