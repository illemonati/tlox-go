package main

import "time"

type Clock struct {
}

func NewClock() *Clock {
	return &Clock{}
}

func (clock *Clock) Arity() int {
	return 0
}

func (clock *Clock) Call(interpreter *Interpreter, arguments []any) any {
	return float64(time.Now().UnixMicro())
}

func (clock *Clock) String() string {
	return "<native code>"
}
