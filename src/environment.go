package main

import "fmt"

type Environment struct {
	vals         map[string]any
	enclosingEnv *Environment
}

func NewEnvironment() *Environment {
	return &Environment{
		vals: make(map[string]any),
	}
}

func NewSubEnvironment(enclosingEnv *Environment) *Environment {
	env := NewEnvironment()
	env.enclosingEnv = enclosingEnv
	return env
}

func (env *Environment) Define(name string, val any) {
	env.vals[name] = val
}

func (env *Environment) GetVariableValue(name Token) (result any) {
	result, ok := env.vals[name.lexeme]

	if !ok {
		if env.enclosingEnv != nil {
			result = env.enclosingEnv.GetVariableValue(name)
		} else {
			Error(name.line, fmt.Sprintf("Variable definition not found for %s.", name.lexeme))
		}
	}

	return
}

func (env *Environment) Assign(name Token, val any) {
	if _, ok := env.vals[name.lexeme]; ok {
		env.vals[name.lexeme] = val
	} else {
		if env.enclosingEnv != nil {
			env.enclosingEnv.Assign(name, val)
		} else {
			Error(name.line, fmt.Sprintf("Undefined variable: %s.", name.lexeme))
		}
	}
}
