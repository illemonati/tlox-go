package main

import "fmt"

type Parser struct {
	tokens  []Token
	current int
}

func NewParser(tokens []Token) *Parser {
	return &Parser{
		tokens:  tokens,
		current: 0,
	}
}

func (p *Parser) parseExpression() Expression {
	return p.parseAssignment()
}

func (p *Parser) parseAssignment() Expression {
	expr := p.parseLogical()

	if p.match(EQUAL) {
		equals := p.previous()
		value := p.parseAssignment()
		if t, ok := expr.(*VariableExpression); ok {
			return NewAssignExpression(t.name, value)
		} else {
			Error(equals.line, "Invalid assignment target.")
		}
	}
	return expr
}

func (p *Parser) parseEquality() Expression {
	expr := p.parseComparison()

	for p.match(BANG_EQUAL, EQUAL_EQUAL) {
		operator := p.previous()
		right := p.parseComparison()
		expr = NewBinaryExpression(expr, right, operator)
	}

	return expr
}

func (p *Parser) Parse() []Statement {
	statements := []Statement{}
	for !p.atEnd() {
		statements = append(statements, p.parseDeclaration())
	}
	return statements
}

func (p *Parser) parseDeclaration() Statement {
	if p.match(VAR) {
		return p.parseVariableDeclaration()
	} else if p.match(FUN) {
		return p.parseFunctionDeclaration("function")
	}
	return p.parseStatement()
}

func (p *Parser) parseFunctionDeclaration(kind string) *FunctionStatement {
	name := p.consume(IDENTIFIER, fmt.Sprintf("Expected %s name.", kind))
	p.consume(LEFT_PAREN, fmt.Sprintf("Expected '(' after %s name.", kind))
	parameters := []Token{}
	if !p.check(RIGHT_PAREN) {
		parameters = append(parameters, p.consume(IDENTIFIER, "Expected parameter name."))
		for p.match(COMMA) {
			parameters = append(parameters, p.consume(IDENTIFIER, "Expected parameter name."))
		}
	}
	p.consume(RIGHT_PAREN, "Expect ')' after parameters.")
	p.consume(LEFT_BRACE, fmt.Sprintf("Expected '{' before %s body.", kind))
	body := p.parseBlockStatement()
	return NewFunctionStatement(name, parameters, body)
}

func (p *Parser) parseVariableDeclaration() Statement {
	name := p.consume(IDENTIFIER, "Expected a variable name.")

	initializer := Expression(nil)
	if p.match(EQUAL) {
		initializer = p.parseExpression()
	}

	p.consume(SEMICOLON, "Expected ';' after variable declaration")
	return NewVariableStatement(name, initializer)
}

func (p *Parser) parseBlockStatement() Statement {
	statements := []Statement{}
	for !p.check(RIGHT_BRACE) && !p.atEnd() {
		statements = append(statements, p.parseDeclaration())
	}
	p.consume(RIGHT_BRACE, "Expected '}' after block declaration.")
	return NewBlockStatement(statements)
}

func (p *Parser) parseStatement() Statement {
	if p.match(PRINT) {
		return p.parsePrintStatement()
	} else if p.match(LEFT_BRACE) {
		return p.parseBlockStatement()
	} else if p.match(IF) {
		return p.parseIfStatement()
	} else if p.match(WHILE) {
		return p.parseWhileStatement()
	} else if p.match(FOR) {
		return p.parseForStatement()
	} else if p.match(RETURN) {
		return p.parseReturnStatement()
	}
	return p.parseExpressionStatement()
}

func (p *Parser) parseReturnStatement() *ReturnStatement {
	keyword := p.previous()
	value := Expression(nil)
	if !p.check(SEMICOLON) {
		value = p.parseExpression()
	}

	p.consume(SEMICOLON, "Expected ';' after return value.")
	return NewReturnStatement(keyword, value)
}

func (p *Parser) parseForStatement() *BlockStatement {
	p.consume(LEFT_PAREN, "Expected '(' after for.")
	initilizer := Statement(nil)
	if p.match(VAR) {
		initilizer = p.parseVariableDeclaration()
	} else if p.match(SEMICOLON) {
		// do nothing
	} else {
		initilizer = p.parseExpressionStatement()
	}

	condition := Expression(nil)
	if !p.check(SEMICOLON) {
		condition = p.parseExpression()
	}
	p.consume(SEMICOLON, "Expected ';' after loop condition.")

	increment := Expression(nil)
	if !p.check(RIGHT_PAREN) {
		increment = p.parseExpression()
	}
	p.consume(RIGHT_PAREN, "Expected ')' after loop increment.")

	body := p.parseStatement()

	result := NewBlockStatement([]Statement{
		initilizer,
		NewWhileStatement(condition, NewBlockStatement([]Statement{
			body, NewExpressionStatement(increment),
		})),
	})

	return result

}

func (p *Parser) parseWhileStatement() *WhileStatement {
	p.consume(LEFT_PAREN, "Expected '(' after while.")
	condition := p.parseExpression()
	p.consume(RIGHT_PAREN, "Expected ')' after condition.")
	body := p.parseStatement()

	return NewWhileStatement(condition, body)
}

func (p *Parser) parseIfStatement() *IfStatement {
	p.consume(LEFT_PAREN, "Expected '(' after if.")
	condition := p.parseExpression()
	p.consume(RIGHT_PAREN, "Expected ')' after if condition.")
	thenBranch := p.parseStatement()
	elseBranch := Statement(nil)

	if p.match(ELSE) {
		elseBranch = p.parseStatement()
	}

	return NewIfStatement(condition, thenBranch, elseBranch)
}

func (p *Parser) parsePrintStatement() *PrintStatement {
	val := p.parseExpression()
	p.consume(SEMICOLON, "Expected '; after value.")
	return NewPrintStatement(val)
}

func (p *Parser) parseExpressionStatement() *ExpressionStatement {
	expr := p.parseExpression()
	p.consume(SEMICOLON, "Expected '; after value.")
	return NewExpressionStatement(expr)
}

func (p *Parser) consume(tokenType TokenType, message string) (result Token) {
	if p.check(tokenType) {
		result = p.readToken()
		return
	}
	Error(p.peek().line, message)
	return
}

func (p *Parser) parsePrimary() (primary Expression) {
	if p.match(FALSE) {
		primary = NewLiteralExpression(false)
	} else if p.match(TRUE) {
		primary = NewLiteralExpression(true)
	} else if p.match(NIL) {
		primary = NewLiteralExpression(nil)
	} else if p.match(NUMBER, STRING) {
		primary = NewLiteralExpression(p.previous().literal)
	} else if p.match(LEFT_PAREN) {
		primary = p.parseExpression()
		p.consume(RIGHT_PAREN, "Expected ')' after expression.")
		primary = NewGroupingExpression(primary)
	} else if p.match(IDENTIFIER) {
		primary = NewVariableExpression(p.previous())
	} else {
		Error(p.peek().line, "Expected expression.")
	}
	return
}

func (p *Parser) parseUnary() Expression {

	if p.match(BANG, MINUS) {
		operator := p.previous()
		right := p.parseUnary()
		return NewUnaryExpression(right, operator)
	}

	return p.parseFunctionCall()
}

func (p *Parser) parseFunctionCall() Expression {
	expr := p.parsePrimary()

	for {
		if p.match(LEFT_PAREN) {
			expr = p.finishCall(expr)
		} else {
			break
		}
	}

	return expr
}

func (p *Parser) finishCall(callee Expression) Expression {
	arguments := make([]Expression, 0)
	if !p.check(RIGHT_PAREN) {
		arguments = append(arguments, p.parseExpression())
		for p.match(COMMA) {
			arguments = append(arguments, p.parseExpression())
		}
	}

	paren := p.consume(RIGHT_PAREN, "Expected ')' after function call arguments.")
	return NewFunctionCallExpression(callee, paren, arguments)
}

func (p *Parser) parseFactor() Expression {
	expr := p.parseUnary()

	for p.match(SLASH, STAR) {
		operator := p.previous()
		right := p.parseUnary()
		expr = NewBinaryExpression(expr, right, operator)
	}

	return expr
}

func (p *Parser) parseTerm() Expression {
	expr := p.parseFactor()

	for p.match(MINUS, PLUS) {
		operator := p.previous()
		right := p.parseFactor()
		expr = NewBinaryExpression(expr, right, operator)
	}

	return expr
}

func (p *Parser) parseComparison() Expression {
	expr := p.parseTerm()

	for p.match(GREATER, GREATER_EQUAL, LESS, LESS_EQUAL) {
		operator := p.previous()
		right := p.parseTerm()
		expr = NewBinaryExpression(expr, right, operator)
	}

	return expr
}

func (p *Parser) parseLogical() Expression {
	expr := p.parseEquality()
	for p.match(OR, AND) {
		operator := p.previous()
		right := p.parseEquality()
		expr = NewBinaryExpression(expr, right, operator)
	}
	return expr
}

func (p *Parser) previous() Token {
	return p.tokens[p.current-1]
}

func (p *Parser) peek() Token {
	return p.tokens[p.current]
}

func (p *Parser) atEnd() bool {
	return p.peek().tokenType == EOF
}

func (p *Parser) readToken() Token {
	if !p.atEnd() {
		p.current++
	}
	return p.previous()
}

func (p *Parser) check(tokenType TokenType) bool {
	if p.atEnd() {
		return false
	}
	return p.peek().tokenType == tokenType
}

func (p *Parser) match(tokenTypes ...TokenType) bool {
	for _, tokenType := range tokenTypes {
		if p.check(tokenType) {
			p.current++
			return true
		}
	}
	return false
}
