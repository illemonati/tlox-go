package main

import "fmt"

type Token struct {
	tokenType TokenType
	lexeme    string
	literal   any
	line      int
}

func MakeToken(tokenType TokenType, lexeme string, literal any, line int) Token {
	return Token{
		tokenType,
		lexeme,
		literal,
		line,
	}
}

func (t Token) String() string {
	return fmt.Sprintf("<%v %s %v>", t.tokenType, t.lexeme, t.literal)
}
