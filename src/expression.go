package main

import "fmt"

type Expression interface {
	String() string
}

type BinaryExpression struct {
	left     Expression
	right    Expression
	operator Token
}

func NewBinaryExpression(left Expression, right Expression, operator Token) *BinaryExpression {
	return &BinaryExpression{
		left, right, operator,
	}
}

func (expr BinaryExpression) String() string {
	return fmt.Sprintf("(%s %s %s)", expr.operator.lexeme, expr.left, expr.right)
}

type GroupingExpression struct {
	innerExpression Expression
}

func NewGroupingExpression(innerExpression Expression) *GroupingExpression {
	return &GroupingExpression{
		innerExpression,
	}
}

func (expr GroupingExpression) String() string {
	return fmt.Sprintf("(%s)", expr.innerExpression)
}

type LiteralExpression struct {
	value any
}

func NewLiteralExpression(value any) *LiteralExpression {
	return &LiteralExpression{
		value,
	}
}

func (expr LiteralExpression) String() string {
	return fmt.Sprintf("%v", expr.value)
}

type UnaryExpression struct {
	right    Expression
	operator Token
}

func NewUnaryExpression(right Expression, operator Token) *UnaryExpression {
	return &UnaryExpression{
		right, operator,
	}
}

func (expr UnaryExpression) String() string {
	return fmt.Sprintf("(%s %s)", expr.operator.lexeme, expr.right)
}

type VariableExpression struct {
	name Token
}

func NewVariableExpression(name Token) *VariableExpression {
	return &VariableExpression{
		name,
	}
}

func (expr VariableExpression) String() string {
	return fmt.Sprintf("<%s>", expr.name.lexeme)
}

type AssignExpression struct {
	name  Token
	value Expression
}

func NewAssignExpression(name Token, value Expression) *AssignExpression {
	return &AssignExpression{
		name, value,
	}
}

func (expr AssignExpression) String() string {
	return fmt.Sprintf("(setq %s %s)", expr.name.lexeme, expr.value)
}

type FunctionCallExpression struct {
	callee    Expression
	paren     Token
	arguments []Expression
}

func NewFunctionCallExpression(callee Expression, paren Token, arguments []Expression) *FunctionCallExpression {
	return &FunctionCallExpression{
		callee, paren, arguments,
	}
}

func (expr FunctionCallExpression) String() string {
	return fmt.Sprintf("(funcall %s %v)", expr.callee, expr.arguments)
}
