package main

import (
	"fmt"
)

type Interpreter struct {
	evaluator         *Evaluator
	globalEnvironment *Environment
	environment       *Environment
}

func NewInterpreter() *Interpreter {
	i := &Interpreter{
		evaluator:   nil,
		environment: NewEnvironment(),
	}
	i.globalEnvironment = i.environment
	i.evaluator = NewEvaluator(i)
	i.initGlobals()
	return i
}

func (i *Interpreter) initGlobals() {
	i.globalEnvironment.Define("clock", NewClock())
}

func (i *Interpreter) interpretExpressionStatement(statement *ExpressionStatement) {
	i.evaluator.Evaluate(statement.expr)
}

func (i *Interpreter) interpretPrintStatement(statement *PrintStatement) {
	val := i.evaluator.Evaluate(statement.expr)
	switch t := val.(type) {
	case float64:
		fmt.Println(NumToString(t))
	default:
		fmt.Println(t)
	}
}

func (i *Interpreter) interpretVariableStatement(statement *VariableStatement) {
	val := any(nil)
	if statement.initializer != nil {
		val = i.evaluator.Evaluate(statement.initializer)
	}

	i.environment.Define(statement.name.lexeme, val)

}

func (i *Interpreter) interpretBlockStatement(statement *BlockStatement) {
	i.interpretBlock(statement.innerStatements, NewSubEnvironment(i.environment))

}

func (i *Interpreter) interpretBlock(statements []Statement, blockEnv *Environment) {
	ogEnv := i.environment

	i.environment = blockEnv

	defer func() {
		if r := recover(); r != nil {
			i.environment = ogEnv
			panic(r)
		}
	}()

	i.Interpret(statements)

	i.environment = ogEnv
}

func (i *Interpreter) interpretIfStatement(statement *IfStatement) {
	if i.evaluator.isTruthy(i.evaluator.Evaluate(statement.condition)) {
		i.Interpret([]Statement{statement.thenBranch})
	} else {
		if statement.elseBranch != nil {
			i.Interpret([]Statement{statement.elseBranch})
		}
	}

}

func (i *Interpreter) interpretWhileStatement(statement *WhileStatement) {
	for i.evaluator.isTruthy(i.evaluator.Evaluate(statement.condition)) {
		i.Interpret([]Statement{statement.body})
	}

}

func (i *Interpreter) interpretFunctionStatement(statement *FunctionStatement) {
	function := NewUserDefinedFunction(statement)
	i.environment.Define(statement.name.lexeme, function)
}

func (i *Interpreter) interpretReturnStatement(statement *ReturnStatement) {
	if statement.value != nil {
		panic(MakeReturnPanic(i.evaluator.Evaluate(statement.value)))
	} else {
		panic(MakeReturnPanic(nil))
	}
}

func (i *Interpreter) Interpret(statements []Statement) {
	for _, statement := range statements {
		if statement == nil {
			continue
		}

		switch t := statement.(type) {
		case *ExpressionStatement:
			i.interpretExpressionStatement(t)
		case *PrintStatement:
			i.interpretPrintStatement(t)
		case *VariableStatement:
			i.interpretVariableStatement(t)
		case *BlockStatement:
			i.interpretBlockStatement(t)
		case *IfStatement:
			i.interpretIfStatement(t)
		case *WhileStatement:
			i.interpretWhileStatement(t)
		case *FunctionStatement:
			i.interpretFunctionStatement(t)
		case *ReturnStatement:
			i.interpretReturnStatement(t)
		default:
			Error(-1, "Unknown statement type, this is a bug.")
		}
	}
}
