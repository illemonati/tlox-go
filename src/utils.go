package main

import (
	"fmt"
	"os"
	"strconv"
)

func EPrintln(args ...any) {
	fmt.Fprintln(os.Stderr, args...)
}

func EPrintf(format string, args ...any) {
	fmt.Fprintf(os.Stderr, format, args...)
}

func DisplayHelp() {
	exename := os.Args[0]
	EPrintf("%s is an implemention of the lox languge interpreter\n", exename)
	EPrintf("Usage: %s <filename>\n", exename)
	EPrintln("filename is recommended to end in the .lox extention")
}

func Error(line int, message string) {
	Report(line, "", message)
}

func Report(line int, where string, message string) {
	EPrintf("[line %d] Error %s: %s", line, where, message)
	os.Exit(65)
}

func NumToString(num float64) string {
	return strconv.FormatFloat(num, 'f', -1, 64)
}
