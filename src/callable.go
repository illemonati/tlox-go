package main

import "fmt"

type Callable interface {
	Call(interpreter *Interpreter, arguments []any) any
	Arity() int
	String() string
}

type ReturnPanic struct {
	returnValue any
}

func MakeReturnPanic(returnValue any) ReturnPanic {
	return ReturnPanic{
		returnValue,
	}
}

type UserDefinedFunction struct {
	declaration *FunctionStatement
}

func NewUserDefinedFunction(declaration *FunctionStatement) *UserDefinedFunction {
	return &UserDefinedFunction{
		declaration,
	}
}

func (f *UserDefinedFunction) Call(interpreter *Interpreter, arguments []any) (returnVal any) {
	functionEnv := NewSubEnvironment(interpreter.globalEnvironment)
	for i, param := range f.declaration.params {
		functionEnv.Define(param.lexeme, arguments[i])
	}

	defer func() {
		if r := recover(); r != nil {
			if t, ok := r.(ReturnPanic); ok {
				returnVal = t.returnValue
			} else {
				panic(r)
			}
		}
	}()

	interpreter.interpretBlock([]Statement{f.declaration.body}, functionEnv)
	return
}

func (f *UserDefinedFunction) Arity() int {
	return len(f.declaration.params)
}

func (f *UserDefinedFunction) String() string {
	return fmt.Sprintf("<user-defined fn %s >", f.declaration.name.lexeme)
}
