package main

type Statement interface{}

type ExpressionStatement struct {
	expr Expression
}

func NewExpressionStatement(expr Expression) *ExpressionStatement {
	return &ExpressionStatement{
		expr,
	}
}

type PrintStatement struct {
	expr Expression
}

func NewPrintStatement(expr Expression) *PrintStatement {
	return &PrintStatement{
		expr,
	}
}

type VariableStatement struct {
	name        Token
	initializer Expression
}

func NewVariableStatement(name Token, initializer Expression) *VariableStatement {
	return &VariableStatement{
		name,
		initializer,
	}
}

type BlockStatement struct {
	innerStatements []Statement
}

func NewBlockStatement(innerStatements []Statement) *BlockStatement {
	return &BlockStatement{
		innerStatements,
	}
}

type IfStatement struct {
	condition  Expression
	thenBranch Statement
	elseBranch Statement
}

func NewIfStatement(condition Expression, thenBranch, elseBranch Statement) *IfStatement {
	return &IfStatement{
		condition, thenBranch, elseBranch,
	}
}

type WhileStatement struct {
	condition Expression
	body      Statement
}

func NewWhileStatement(condition Expression, body Statement) *WhileStatement {
	return &WhileStatement{
		condition, body,
	}
}

type FunctionStatement struct {
	name   Token
	params []Token
	body   Statement
}

func NewFunctionStatement(name Token, params []Token, body Statement) *FunctionStatement {
	return &FunctionStatement{
		name, params, body,
	}
}

type ReturnStatement struct {
	keyword Token
	value   Expression
}

func NewReturnStatement(keyword Token, value Expression) *ReturnStatement {
	return &ReturnStatement{
		keyword, value,
	}
}
