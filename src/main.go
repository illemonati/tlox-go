package main

import (
	"os"
)

func main() {
	arglen := len(os.Args)
	// arg[0] is name
	if arglen == 2 {
		runOnFile(os.Args[1])
	} else {
		DisplayHelp()
	}
}

func runOnFile(filename string) {
	fileBytes, err := os.ReadFile(filename)
	if err != nil {
		EPrintln("Unable to open file")
		os.Exit(65)
	}
	contents := string(fileBytes)
	scanner := MakeScanner(contents)
	tokens := scanner.ScanTokens()
	// fmt.Println(tokens)

	parser := NewParser(tokens)
	statements := parser.Parse()
	// fmt.Println(expr)

	interpreter := NewInterpreter()
	interpreter.Interpret(statements)
}
